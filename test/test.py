import json


class Serializable(object):
    def json(self):
        return json.dumps(self, default=lambda o: o.__dict__)


class A(Serializable):
    def __init__(self):
        self.a = "aga"

    @staticmethod
    def testt(**args):
        return [args]

a1 = A()
print(json.dumps(A.testt(1,2,3, a=4,b=5,c=6)))
