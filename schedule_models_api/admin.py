from django.contrib import admin

from .models import LessonItem


admin.site.register(LessonItem)
# Register your models here.
