from django.apps import AppConfig


class ScheduleModelsApiConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'schedule_models_api'
