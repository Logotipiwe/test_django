

class LessonObject:
    def __init__(self, date, lesson_position, group_num, title, teacher, cabinet, lesson_type):
        self.title = title
        self.teacher = teacher
        self.cabinet = cabinet
        self.type = lesson_type
        self.date = date
        self.group_num = group_num
        self.lesson_position = lesson_position

    def __str__(self):
        return "t: {}. gn: {}. ln: {}. d: {}. tchr: {}".format(
            self.title, self.group_num, self.lesson_position,
            self.date, self.teacher)
