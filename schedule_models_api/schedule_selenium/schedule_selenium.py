import datetime
import json

from django.http import HttpResponse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

from .models import LessonObject


def extract_str_from_lesson(l, className):
    elem = l.find_element(By.CLASS_NAME, className)
    return elem.text if elem is not None else "NO_DATA"


def get_rasp_table(d):
    dates = get_dates(d)
    print("dates", dates)
    lessons = []
    for day in range(len(dates)):
        date = dates[day]
        day_of_week = str(day + 1)
        print("day {}".format(day_of_week))
        column = d.find_elements(By.CLASS_NAME, "rasp-day" + day_of_week)
        for lesson in column:
            row = lesson.find_element(By.XPATH, '..')
            lesson_position = row.find_element(By.CLASS_NAME, "vt283").text
            lesson_groups = lesson.find_elements(By.CLASS_NAME, "vt258")
            for group_num in range(len(lesson_groups)):
                lesson_group = lesson_groups[group_num]
                title = extract_str_from_lesson(lesson_group, "vt240")
                teacher = extract_str_from_lesson(lesson_group, "teacher")
                cabinet = extract_str_from_lesson(lesson_group, "vt242")
                lesson_type = extract_str_from_lesson(lesson_group, "vt243")
                lesson_obj = LessonObject(
                    date=date,
                    lesson_position=lesson_position,
                    group_num=group_num,
                    title=title,
                    teacher=teacher,
                    cabinet=cabinet,
                    lesson_type=lesson_type)
                lessons.append(lesson_obj)
                print(lesson_obj)
    return lessons


def get_dates(d):
    dates_tags = d.find_elements(by=By.CLASS_NAME, value="vt237")
    dates = list(map(lambda t: t.text.split('\n')[0] + '.2022', dates_tags[1::]))
    return list(map(lambda da: datetime.datetime.strptime(da, '%d.%m.%Y'), dates))


def parse_table_cell(cell):
    return [x.text for x in cell.find_elements(By.CLASS_NAME, "vt258")]


def do_parse(request=None):
    main_options = Options()
    main_options.add_argument('--headless')
    main_driver = webdriver.Chrome(options=main_options)
    main_driver.get(
        "https://www.sut.ru/studentu/raspisanie/raspisanie-zanyatiy-studentov-ochnoy-i-vecherney-form-obucheniya")
    try:
        groups = main_driver.find_elements(by=By.CLASS_NAME, value="vt256")
        for group in groups[0:1]:
            href = group.get_attribute('href')
            options = Options()
            options.add_argument("--window-size=1920,1080")
            options.add_argument('--headless')
            driver = webdriver.Chrome(options=options)
            driver.get(href)
            try:
                lessons = get_rasp_table(driver)
                print("Это успех")
                return lessons
            finally:
                print("Closing page...")
                driver.close()
                driver.quit()
                print("Page closed!")
    finally:
        print("Closing selenium...")
        main_driver.close()
        main_driver.quit()
        print("selenium closed!")
