from django.http import HttpResponse
from django.urls import path

from .models import LessonItem
from .schedule_selenium import do_parse


def test(request):
    return LessonItem.objects.create_lesson(title="lol")


urlpatterns = [
    path('parse/', do_parse),
    path('test/', test)
]
