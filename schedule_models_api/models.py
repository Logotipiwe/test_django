from django.db import models


class LessonManager(models.Manager):
    # def create(self, title, teacher, cabinet, type, date, group_num, lesson_pos, lesson):
    def create_lesson(self, title):
        lesson = self.create(title=title)
        return lesson


class LessonItem(models.Model):
    title = models.CharField(max_length= 1000)
    teacher = models.CharField(max_length= 500)
    cabinet = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    date = models.DateField()
    group_num = models.IntegerField()
    lesson_pos = models.CharField(max_length=255)

    objects = LessonManager()

    @staticmethod
    def from_lesson_object(lesson_obj):
        return LessonItem()
    # def __init__(self, date, lesson_num, group_num, title, teacher, cabinet, lesson_type):
    #     super.
    #     self.title = title
    #     self.teacher = teacher
    #     self.cabinet = cabinet
    #     self.type = lesson_type
    #     self.date = date
    #     self.group_num = group_num
    #     self.lesson_num = lesson_num
    #
    # def __str__(self):
    #     return "t: {}. gn: {}. ln: {}. d: {}. tchr: {}".format(self.title, self.group_num, self.lesson_num,
    #                                                            self.date, self.teacher)